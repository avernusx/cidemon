# Use the AWS Elastic Beanstalk Python 3.4 image
# original file here https://github.com/aws/aws-eb-python-dockerfiles/blob/master/3.4.2-aws-eb-onbuild/Dockerfile

FROM       python:3.4.2

ENV        UWSGI_NUM_PROCESSES    1
ENV        UWSGI_NUM_THREADS      15
ENV        UWSGI_UID              uwsgi
ENV        UWSGI_GID              uwsgi
ENV        UWSGI_LOG_FILE         /var/log/uwsgi/uwsgi.log
ENV        DOCKER                 1

ENV        WSGI_PATH              app.py

WORKDIR    /var/app
ADD . /var/app

RUN        pip install -r /var/app/requirements.txt
RUN        useradd uwsgi -s /bin/false
RUN        mkdir /var/log/uwsgi
RUN        chown -R uwsgi:uwsgi /var/log/uwsgi


EXPOSE     8080

ADD        uwsgi-start.sh /

CMD        []

RUN ["chmod", "+x", "./uwsgi-start.sh"]
ENTRYPOINT ["./uwsgi-start.sh"]


